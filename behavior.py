import random
from mechanics import Calculation


class Behavior:

    def determine_behavior(self, location):
        if self.attitude == 'aggresive':
            return Fight.perform(self, location)


class Fight(Behavior):

    def perform(self, location):
        target = location.get_random_player()
        throw = Calculation.calculate_attack(self)
        if throw >= target.armor_klass:
            target.health -= target.weapon_of_choice.damage
            print(self.name + ' atakuje Cię za pomocą ' + self.weapon_of_choice.name + ' i zadaje Ci ' + str(
                self.weapon_of_choice.damage) + ' punktów obrażeń.')
        else:
            print(self.name + ' Cię atakuje ale nie trafia.')