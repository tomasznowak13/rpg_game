import sys
from action import *
from character_classes import *
from screen import *
from action import Action


class Character:
    def __init__(self, name, klass):
        self.name = name
        self.klass = globals()[klass]()
        self.health = 100
        self.inventory = [Sword(), Dagger(), GoldCoin()]
        self.weapon_of_choice = None
        self.level = 1
        self.armor_klass = 10

    def is_alive(self):
        return self.health > 0

    def get_health(self):
        return self.health

    def get_inventory(self):
        return self.inventory


class Player(Character):
    def __init__(self, name, klass):
        Character.__init__(self, name, klass)
        self.weapon_of_choice = Sword()

    def perform_action(self, location):
        available_actions = Action.get_actions()
        Screen.display_choices(available_actions)
        action = Screen.get_player_action()
        while Action.is_action_validate(action):
            if available_actions[action].is_free:
                available_actions[action].perform(self, location)
                continue
            else:
                available_actions[action].perform(self, location)
                break