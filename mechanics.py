import random
from character_data import *


class Throw:

    @staticmethod
    def test_attack():
        return random.randint(1, 20)


class Calculation:

    @staticmethod
    def calculate_attack(character):
        throw = Throw.test_attack()
        attribute_modifier = Modifiers.get_attribute_modifier(character.klass.strength)
        base_bonus = LevelBonuses.get_attack_bonus(character.level)
        score = throw + attribute_modifier + base_bonus
        return score