from location import *
from npcs import *


class World:
    def __init__(self):
        self.locations =[]

    def create_locations(self):
        self.locations = [
            Room(characters=[],
                 items=[Desk(), Wallet()]),
            Hallway(characters=[Undead(1), Troll(1)],
                    items=[]),
            Hallway(characters=[],
                    items=[]),
            Hallway(characters=[Undead(1)],
                    items=[Sack()]),
            Room(characters=[Troll(1), Undead(1)],
                 items=[Crate()]),
            Room(characters=[HollowKnight(1)],
                 items=[Desk()]),
            Hallway(characters=[],
                    items=[Wallet()]),
            Hallway(characters=[Troll(1), Undead(1)],
                    items=[Wallet()]),
            Hallway(characters=[],
                    items=[]),
            Hallway(characters=[Troll(1)],
                    items=[]),
            Room(characters=[Undead(1)],
                 items=[Sack()]),
            Room(characters=[Necromancer(1), Undead(1)],
                 items=[Wallet(), BigCrate()]),
            Dungeon(characters=[],
                    items=[]),
            Dungeon(characters=[],
                    items=[Wallet()]),
            Dungeon(characters=[Undead(1), Undead(1)],
                    items=[Crate()]),
            Dungeon(characters=[Necromancer(1), Troll(1)],
                    items=[BigCrate()]),
            Room(characters=[],
                 items=[Cauldron()]),
            ]

    def set_transitions(self):
        self.locations[0].transitions['north'] = self.locations[1]

        self.locations[1].transitions['west'] = self.locations[2]
        self.locations[1].transitions['south'] = self.locations[0]

        self.locations[2].transitions['west'] = self.locations[3]
        self.locations[2].transitions['east'] = self.locations[1]

        self.locations[3].transitions['north'] = self.locations[4]
        self.locations[3].transitions['east'] = self.locations[2]

        self.locations[4].transitions['north'] = self.locations[7]
        self.locations[4].transitions['west'] = self.locations[5]
        self.locations[4].transitions['south'] = self.locations[3]
        self.locations[4].transitions['down'] = self.locations[12]

        self.locations[5].transitions['north'] = self.locations[6]
        self.locations[5].transitions['east'] = self.locations[4]

        self.locations[6].transitions['south'] = self.locations[5]
        self.locations[6].transitions['east'] = self.locations[7]

        self.locations[7].transitions['south'] = self.locations[4]
        self.locations[7].transitions['west'] = self.locations[6]
        self.locations[7].transitions['east'] = self.locations[8]

        self.locations[8].transitions['west'] = self.locations[7]
        self.locations[8].transitions['east'] = self.locations[9]

        self.locations[9].transitions['west'] = self.locations[8]
        self.locations[9].transitions['south'] = self.locations[10]

        self.locations[10].transitions['north'] = self.locations[9]
        self.locations[10].transitions['west'] = self.locations[11]

        self.locations[12].transitions['south'] = self.locations[13]
        self.locations[12].transitions['top'] = self.locations[4]

        self.locations[13].transitions['east'] = self.locations[14]
        self.locations[13].transitions['north'] = self.locations[12]

        self.locations[14].transitions['west'] = self.locations[13]
        self.locations[14].transitions['south'] = self.locations[15]

        self.locations[15].transitions['north'] = self.locations[14]
        self.locations[15].transitions['top'] = self.locations[16]

        self.locations[16].transitions['down'] = self.locations[15]

    def has_location_enemy(self, location):
        if location.characters:
            for character in location:
                if character.attitude == 'aggresive':
                    return True
                    break
        else:
            return False

    def remove_character(self, location, npc):
        character_index = self.locations[location].characters.index(npc)
        self.locations[location].characters.pop(character_index)