from character import *
from character_classes import *
from weapons import *
import random
from behavior import *


class NpcCharacter(Character):
    def __init__(self, name, klass, attitude):
        Character.__init__(self, name, klass)
        self.attitude = attitude

    def perform_action(self, location):
        Behavior.determine_behavior(self, location)

    def __repr__(self):
        return repr(self.name)


class Troll(NpcCharacter):
    def __init__(self, level, name='Troll', klass='Barbarian', inventory=[Club()], attitude='aggresive', armor_klass = 12):
        NpcCharacter.__init__(self, name, klass, attitude)
        self.name = name
        self.klass = globals()[klass]()
        self.inventory = inventory
        self.attitude = attitude
        self.weapon_of_choice = self.inventory[0]
        self.level = level
        self.armor_klass = armor_klass


class Necromancer(NpcCharacter):
    def __init__(self, level, name='Necromancer', klass='Wizard', inventory=[Wand(), Dagger()], attitude='aggresive', armor_klass = 7):
        NpcCharacter.__init__(self, name, klass, attitude)
        self.name = name
        self.klass = globals()[klass]()
        self.inventory = inventory
        self.attitude = attitude
        self.weapon_of_choice = self.inventory[0]
        self.level = level
        self.armor_klass = armor_klass


class HollowKnight(NpcCharacter):
    def __init__(self, level, name='HollowKnight', klass='Knight', inventory=[GreatSword()], attitude='passive', armor_klass = 9):
        NpcCharacter.__init__(self, name, klass, attitude)
        self.name = name
        self.klass = globals()[klass]()
        self.inventory = inventory
        self.attitude = attitude
        self.weapon_of_choice = self.inventory[0]
        self.level = level
        self.armor_klass = armor_klass


class Undead(NpcCharacter):
    def __init__(self, level, name='Undead', klass='Warrior', inventory=[Sword()], attitude='aggresive', armor_klass = 10):
        NpcCharacter.__init__(self, name, klass, attitude)
        self.name = name
        self.klass = globals()[klass]()
        self.inventory = inventory
        self.attitude = attitude
        self.weapon_of_choice = self.inventory[0]
        self.level = level
        self.armor_klass = armor_klass