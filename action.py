import sys
from weapons import *
from world import *
from npcs import *
from mode import *
from mechanics import *
from screen import *
from game import *


class Action:

    is_free = False

    @classmethod
    def get_actions(cls):
        actions = [action for action in Action.__subclasses__()]
        return actions

    @classmethod
    def is_action_validate(cls, action):
        available_actions = cls.get_actions()
        try:
            int(action)
            if action in range(len(available_actions)):
                return True
            else:
                print('Liczba spoza zakresu')
        except:
            print('Wprowadź liczbę')


class Move(Action):

    name = 'Move'
    mode = [Mode.default, Mode.fight]

    def perform(self, location):
        move_directions = location.get_available_transitions()
        Screen.display_choices(move_directions)
        move_direction = int(input('W którym kierunku chciałbyś się przemieścić?\n'))
        if move_direction in range(len(move_directions)):
            Game.assign_player_to_location(location.transitions[move_directions[move_direction]])
            print('Przeszedłeś do kolejnego pomieszczenia')
        else:
            print('Nie ma takiego kierunku')


class Fight(Action):

    name = 'Fight'
    mode = [Mode.fight]

    def perform(self, location):
        enemies = location.get_location_enemies()
        print('Dostępny wybór: ')
        Screen.display_choices(enemies)
        enemy = int(input('Kogo chcesz zaatakować?\n'))
        if int(enemy) in range(len(enemies)):
            enemy = enemies[int(enemy)]
            throw = Calculation.calculate_attack(self)
            if throw >= enemy.armor_klass:
                enemy.health -= self.weapon_of_choice.damage
                print('Atakujesz ' + enemy.name + ' za pomocą ' + self.weapon_of_choice.name + ' i zadajesz mu ' + str(
                        self.weapon_of_choice.damage) + ' punktów obrażeń.')
            else:
                print("Atakujesz przeciwnika, ale go nie trafiasz")
        else:
            print('Nie ma takiego przeciwnika')


class EquipWeapon(Action):

    name = 'Equip Weapon'
    mode = [Mode.default, Mode.fight]

    def perform(self, location):
        weapons = [weapon for weapon in self.get_inventory() if isinstance(weapon, Weapon)]
        print('Broń do wyboru:')
        Screen.display_choices(weapons)
        player_choice = int(input('Którą broń wybierasz? '))
        if int(player_choice) in range(len(weapons)):
            self.weapon_of_choice = weapons[int(player_choice)]
            print('Twoja aktualna broń to ' + self.weapon_of_choice.name)
        else:
            print('Nie ma takiej broni')


class Status(Action):

    name = 'Status'
    mode = [Mode.default, Mode.fight]
    is_free = True

    def perform(self):
        health = self.get_health()
        inventory = self.get_inventory()
        print('##################################################')
        print('Twoje zdrowie wynosi ' + str(health) + ' punktów życia.')
        print('Twój inwentarz:')
        for item in inventory:
            print(item)
        print('Twoja główna broń to: ' + str(self.weapon_of_choice))
        print('##################################################')


class Explore(Action):

    name = 'Explore'
    mode = [Mode.default]

    def perform(self, location):
        print('Przedmioty w pomieszczeniu:')
        [print(item) for item in location.items]


class Take(Action):

    name = 'Take'
    mode = [Mode.default]

    def perform(self, location):
        items = [item for item in location.items]
        if items:
            print('Przedmioty:')
            Screen.display_choices(items)
            player_choice = int(input('Który przedmiot chcesz podnieść? '))
            if int(player_choice) in range(len(items)):
                self.inventory.append(items[int(player_choice)])
                self.position.items.remove(items[int(player_choice)])
                print('Dodałeś ' + items[int(player_choice)].name + ' do swojego ekwipunku.')
            else:
                print('Nie ma takiego przedmiotu')
        else:
            print('Nie ma żadnych [rz')