from items import *
import random


class Weapon(Item):
    def __init__(self, name, description, value):
        Item.__init__(self, name, description)
        self.value = value

    def decrease_condition(self, value):
        self.condition -= value

    @classmethod
    def get_random_weapon(cls):
        weapon = [Sword(), Dagger(), Club(), GreatSword(),
                  Knife(), BrokenSword(), Stick(), Katana()]
        return random.choice(weapon)


class Sword(Weapon):
    def __init__(self, name='Sword', description='Made with famous steel from far lands', value=50, damage=8, condition=100):
        Weapon.__init__(self, name, description, value)
        self.name = name
        self.description = description
        self.value = value
        self.damage = damage
        self.condition = condition


class Dagger(Weapon):
    def __init__(self, name='Dagger', description='Make a quick stab', value=35, damage=4, condition=100):
        Weapon.__init__(self, name, description, value)
        self.name = name
        self.description = description
        self.value = value
        self.damage = damage
        self.condition = condition


class Club(Weapon):
    def __init__(self, name='Club', description='Smash your enemies', value=20, damage=12, condition=100):
        Weapon.__init__(self, name, description, value)
        self.name = name
        self.description = description
        self.value = value
        self.damage = damage
        self.condition = condition


class GreatSword(Weapon):
    def __init__(self, name='Great Sword', description='Two-handed big sword', value=60, damage=40, condition=100):
        Weapon.__init__(self, name, description, value)
        self.name = name
        self.description = description
        self.value = value
        self.damage = damage
        self.condition = condition


class Knife(Weapon):
    def __init__(self, name='Knife', description='', value=10, damage=12, condition=100):
        Weapon.__init__(self, name, description, value)
        self.name = name
        self.description = description
        self.value = value
        self.damage = damage
        self.condition = condition


class BrokenSword(Weapon):
    def __init__(self, name='Broken Sword', description='', value=2, damage=3, condition=20):
        Weapon.__init__(self, name, description, value)
        self.name = name
        self.description = description
        self.value = value
        self.damage = damage
        self.condition = condition


class Stick(Weapon):
    def __init__(self, name='Stick', description='Wooden stick', value=0, damage=2, condition=100):
        Weapon.__init__(self, name, description, value)
        self.name = name
        self.description = description
        self.value = value
        self.damage = damage
        self.condition = condition


class Katana(Weapon):
    def __init__(self, name='Katana', description='', value=45, damage=30, condition=100):
        Weapon.__init__(self, name, description, value)
        self.name = name
        self.description = description
        self.value = value
        self.damage = damage
        self.condition = condition


class Wand(Weapon):
    def __init__(self, name='Wand', description='Mysterious wand with hidden power', value=150, damage=60, condition=100):
        Weapon.__init__(self, name, description, value)
        self.name = name
        self.description = description
        self.value = value
        self.damage = damage
        self.condition = condition