class Item:
    def __init__(self, name, description):
        self.name = name
        self.description = description

    def __repr__(self):
        return repr(self.name)

    def __str__(self):
        return self.name


class GoldCoin(Item):
    def __init__(self):
        self.name = 'Gold Coin'
        self.description = 'Basic means of payment'
        self.value = 1