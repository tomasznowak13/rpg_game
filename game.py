import random
from world import *
from action import *
from character import Player


class Game:
    def __init__(self):
        world = self.create_world()
        self.player = self.create_player()
        self.assign_player_to_location(world.locations[1])

    def create_world(self):
        self.world = World()
        self.world.create_locations()
        self.world.set_transitions()
        return self.world

    def assign_player_to_location(self, location):
        location.characters.append(self.player)

    def create_player(self):
        name = input("Podaj imię swojego bohatera\n")
        klass = input("Podaj klasę postaci jaka chcesz zagrać\nDostępne klasy to: Knight, Wizard, Barbarian, Warrior\n")
        player = Player(name=name, klass=klass)
        return player

    @staticmethod
    def shuffle_characters(location):
        characters = location.characters
        random.shuffle(characters)
        return characters

    #@staticmethod
    #def get_random_player(location):
        #players = [player for player in location.characters if isinstance(player, Player)]
        #return random.choice(players)

    def play(self):
        print("Witaj w grze " + self.player.name + ", znajdujesz się w nieznanym miejscu w ciemnym pokoju.\nTwoim zadnaiem jest wydostać się na zewnątrz. Powodzenia!")
        while self.player.is_alive():
            for location in self.world.locations:
                if self.player in location.characters:
                    shuffled_characters = self.shuffle_characters(location)
                    for character in shuffled_characters:
                        character.perform_action(location)
        print("Niestety nie żyjesz")
