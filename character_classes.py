class CharacterKlass:
    def __init__(self):
        self.strength = 10
        self.dexterity = 10
        self.endurance = 10
        self.intelligence = 10
        self.wisdom = 10
        self.charisma = 10

    def __repr__(self):
        return repr(self.name)


class Knight(CharacterKlass):
    def __init__(self):
        self.name = 'Knight'
        self.strength = 12
        self.dexterity = 11
        self.endurance = 12
        self.charisma = 9


class Wizard(CharacterKlass):
    def __init__(self):
        self.name = 'Wizard'
        self.strength = 7
        self.endurance = 8
        self.intelligence = 12
        self.wisdom = 12


class Barbarian(CharacterKlass):
    def __init__(self):
        self.name = 'Barbarian'
        self.strength = 14
        self.endurance = 13
        self.intelligence = 5
        self.wisdom = 6
        self.charisma = 6


class Warrior(CharacterKlass):
    def __init__(self):
        self.name = 'Warrior'
        self.strength = 11
        self.dexterity = 12
        self.endurance = 11
        self.intelligence = 7
        self.wisdom = 7
        self.charisma = 7