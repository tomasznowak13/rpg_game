from action import *


class Screen:
    @staticmethod
    def get_player_action():
        selected_action = int(input("Co chcesz zrobić?\n"))
        return selected_action

    @staticmethod
    def display_choices(choices):
        print("\nDostępne akcje:")
        try:
            return [print(str(choices.index(choice)) + ' - ' + choice.name) for choice in choices]
        except:
            return [print(str(choices.index(choice)) + ' - ' + choice) for choice in choices]
