from items import *
from weapons import *

"""Klasa Content określa przedmiot, który posiada właściwość przechowywania innych
przedmiotów"""
class Content(Item):
    def __init__(self):
        self.inventory = []

    def generate_inventory(self, type, min_items, max_items):
        items_amount = random.randint(min_items, max_items)
        if type == 'weapons':
            for item in range(items_amount):
                weapon = Weapon.get_random_weapon()
                self.inventory.append(weapon)
        elif type == 'coins':
            for item in range(items_amount):
                self.inventory.append(GoldCoin())


class Crate(Content):
    def __init__(self):
        Content.__init__(self)
        self.name = 'Crate'
        self.description = 'Contains interesting items'
        self.generate_inventory('weapons', 1, 4)


class BigCrate(Content):
    def __init__(self):
        Content.__init__(self)
        self.name = 'Big crate'
        self.description = 'Contains a lot of interesting items'
        self.generate_inventory('weapons', 3, 6)


class Sack(Content):
    def __init__(self):
        Content.__init__(self)
        self.name = 'Sack'
        self.description = 'Contains some items'
        self.generate_inventory('weapons', 0, 2)


class Wallet(Content):
    def __init__(self):
        Content.__init__(self)
        self.name = 'Wallet'
        self.description = 'Leather wallet'
        self.generate_inventory('coins', 1, 6)


class Desk(Content):
    def __init__(self):
        Content.__init__(self)
        self.name = 'Desk'
        self.description = 'Old decorated desk'
        self.generate_inventory('weapons', 1, 4)


class Cauldron(Content):
    def __init__(self):
        Content.__init__(self)
        self.name = 'Cauldron'
        self.description = 'Old cauldron made of gold by ancient dwarfs'
        self.generate_inventory('coins', 50, 100)