from content import *
from npcs import *
from character import *


class Location:
    def __init__(self, characters, items):
        self.characters = characters
        self.items = items
        self.transitions = {'north': None, 'south': None, 'west': None, 'east': None, 'top': None, 'down': None}

    def __repr__(self):
        return repr(self.name)

    def get_location_enemies(self):
        enemies = []
        for character in self.characters:
            if isinstance(character, NpcCharacter)and character.attitude == 'aggresive':
                enemies.append(character)
        return enemies

    def get_available_transitions(self):
        return [transition for transition in self.transitions if self.transitions[transition] is not None]

    def get_random_player(self):
        players = [player for player in self.characters if isinstance(player, Player)]
        return random.choice(players)


class Room(Location):
    def __init__(self, characters, items):
        Location.__init__(self, characters, items)
        self.name = 'Room'


class Hallway(Location):
    def __init__(self, characters, items):
        Location.__init__(self, characters, items)
        self.name = 'Hallway'


class Dungeon(Location):
    def __init__(self, characters, items):
        Location.__init__(self, characters, items)
        self.name = 'Dungeon'


