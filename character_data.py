class Modifiers:

    @staticmethod
    def get_attribute_modifier(value):
        if value in range(2):
            return -5
        elif value in range(2, 4):
            return -4
        elif value in range(4, 6):
            return -3
        elif value in range(6, 8):
            return -2
        elif value in range(8, 10):
            return -1
        elif value in range(10, 12):
            return 0
        elif value in range(12, 14):
            return 1
        elif value in range(14, 16):
            return 2
        elif value in range(16, 18):
            return 3
        elif value in range(18, 20):
            return 4
        elif value in range(20, 22):
            return 5


class LevelBonuses:
    attack_bonuses = {
        1: 2,
        2: 3,
        3: 4,
        4: 5,
        5: 6,
    }

    @classmethod
    def get_attack_bonus(cls, level):
        return cls.attack_bonuses[level]